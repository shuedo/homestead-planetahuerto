# homestead-planetahuerto

## Instalación

1. Modificar la ruta de Homestead.yaml para que se corresponda con el directorio del proyecto.
2. Lanzar `vagrant up --provision`.
3. Añadir al /etc/hosts `192.168.10.10 planetahuerto.test`
4. Entrar a la máquina con `vagrant ssh`.
5. Ejecutar `composer install` desde la carpeta `/home/vagrant/planetahuerto`.
