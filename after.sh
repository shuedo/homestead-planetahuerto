#### ENABLE XDEBUG
sudo phpenmod xdebug

if grep -Fxq "xdebug.remote_autostart = 1" /etc/php/7.4/cli/conf.d/20-xdebug.ini
        then
            echo 'Ya esta activado php-xdebug para php-cli'
        else
            echo 'xdebug.remote_autostart = 1' | sudo tee -a /etc/php/7.4/cli/conf.d/20-xdebug.ini
fi

if grep -Fxq "xdebug.remote_autostart = 1" /etc/php/7.4/fpm/conf.d/20-xdebug.ini
        then
            echo 'Ya esta activado php-xdebug para php-fpm'
        else
            echo 'xdebug.remote_autostart = 1' | sudo tee -a /etc/php/7.4/fpm/conf.d/20-xdebug.ini
            sudo /etc/init.d/php7.4-fpm restart
fi

sudo update-alternatives --set php /usr/bin/php7.4
sudo update-alternatives --set php-config /usr/bin/php-config7.4
sudo update-alternatives --set phpize /usr/bin/phpize7.4