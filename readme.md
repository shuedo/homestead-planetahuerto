# Instalación de Homestead
1. Instalar VirtualBox
2. Instalar Vagrant
3. Configurar Vagrant
    * vagrant box add laravel/homestead
    * Clonar en el directorio raíz el Homestead: `git clone https://github.com/laravel/homestead.git ~/Homestead`
    * Dentro de Homestead, `git checkout release` para trabajar con la última versión estable.
    * Una vez configurado el Homestead.yaml, lanzar en el directorio de Homestead `bash init.sh`
5. Configuramos en el /etc/hosts los sites de Homestead con la ip correspondiente.
4. Lanzamos vagrant up y a trabajar!!

## Requerimientos necesarios para BDU:
#### INSTALL KAFKA EXTENSION
sudo apt install librdkafka-dev

sudo pecl install rdkafka

Añadir la extensión al php.ini : /etc/php/7.4/conf.d/ -> extension=rdkafka.so

## Instalación de mysql dentro de Homestead
Para el uso de mysql en la imagen de Vagrant, solamente tendremos que activarlo a true como nos indica la documentación: https://laravel.com/docs/5.8/homestead#installing-optional-features

Una vez reiniciemos Vagrant con `vagrant up --provision`, podremos utilizar todas las bases de datos que tengamos instanciadas en el apartado 'databases' del Homestead.yaml.

### Uso desde cliente
Para utilizar un cliente de mysql o para utilizar una base de datos desde nuestras aplicaciones, nos conectaremos a través del puerto 33060.

Otra forma de conectar a un cliente si la primera no funciona, es conectar mediante SSH con las siguientes opciones por defecto:
```
SSH Hostname: 192.168.10.10
SSH Username: vagrant
SSH Password: vagrant
MYSQL Hostname: 127.0.0.1
MYSQL Server port: 3306
Username: homestead
Password: secret
```

## Instalación de mongo dentro de Homestead
Para el uso de mongo en la imagen de Vagrant, solamente tendremos que activarlo a true como nos indica la documentación: https://laravel.com/docs/5.7/homestead#installing-mongodb

Una vez reiniciemos Vagrant con `vagrant up --provision`, podremos utilizar todas las bases de datos que tengamos instanciadas en el apartado 'databases' del Homestead.yaml

### Uso desde cliente
Para utilizar un cliente de mongo o para utilizar una colección desde nuestras aplicaciones, crearemos un usuario asociado a la db que queramos, con este comando desde dentro de vagrant y en una terminal de mongo:

```db.createUser({ user: "homestead", pwd: "secret", roles: [{ role: "readWrite", db: "coleccion_a_la_que_queramos_asociar_el_usuario" }] })```

tip: para conectarse a cliente, es aconsejable deshabilitar la conexión por SSL, por el puerto 27017.

TODO:
* Reiniciar automáticamente el servicio de elasticsearch, ahora mismo es a mano